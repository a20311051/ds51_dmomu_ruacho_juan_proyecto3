import 'package:flutter/material.dart';
import 'package:pokeapi/home.dart';
import 'package:pokeapi/item.dart';
import 'package:pokeapi/move.dart';
import 'package:pokeapi/region.dart';
import 'package:pokeapi/type.dart';
import 'data/models/pokemon.dart';
import 'vistaPokemon.dart';

void main() {
  runApp(const PokeApi());
}

class PokeApi extends StatelessWidget {
  const PokeApi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Poke API',
      theme: ThemeData(
        primarySwatch: Colors.red,
        brightness: Brightness.light,
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.red,
      ),
      themeMode: ThemeMode.system,
      home: const Home(title: 'Poke API'),
      routes: {
        '/home': (context) => const Home(title: 'Home'),
        '/move': (context) => Move(move: 0),
        '/pokemon': (context) => const Pokemon(pokemon: 0),
        '/region': (context) => const Region(region: ''),
        '/type': (context) => const Type(type: ''),
        '/item': (context) => const Item(item: 0),
'/vistaPokemon': (context) =>  VistaPokemon(vista: 'Valor de vista', pokemon: {}, moves: [], pokemonNumber: 0,),
      },
    );
  }
}
