import 'package:flutter/material.dart';

import 'network/pokeService.dart';

class Region extends StatefulWidget {
  const Region({super.key, required this.region});

  final String   region;

  @override
  State<Region> createState() => _RegionState();

}

class _RegionState extends State<Region> {
  List<dynamic> _pokemonList = [];

  @override
  void initState() {
    super.initState();
    _fetchPokemonList();
  }

  void _fetchPokemonList() async {
    try {
      final regionData = await PokeService.getRegionData(widget.region);
      final pokemonList = regionData['pokemon_species'];
      setState(() {
        _pokemonList = pokemonList;
      });
    } catch (e) {
      print('Error fetching region data: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pokémon in ${widget.region}'),
      ),
      body: ListView.builder(
        itemCount: _pokemonList.length,
        itemBuilder: (context, index) {
          final pokemon = _pokemonList[index];
          return ListTile(
            title: Text(pokemon['name']),
            onTap: () {
              Navigator.pushNamed(context, '/pokemon', arguments: pokemon['url']);
            },
          );
        },
      ),
    );
  }
}
