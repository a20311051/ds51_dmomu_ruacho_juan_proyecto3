import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:pokeapi/network/pokeService.dart';

import 'home.dart';

class VistaPokemon extends StatefulWidget {
  final dynamic pokemon;
  final dynamic moves;
  final int pokemonNumber;

  const VistaPokemon({
    Key? key, // Cambio en esta línea
    required this.pokemon,
    this.moves,
    required this.pokemonNumber,
    String? vista,
  }) : super(key: key);

  @override
  VistaPokemonState createState() => VistaPokemonState();
}

class VistaPokemonState extends State<VistaPokemon> {
  Future<Map<String, dynamic>>? pokemonDetails;
  List<Map<String, dynamic>> evolutionsList = [];
  List<dynamic> _moves = [];
  String pokemonType = 'Unknown';

  @override
  void initState() {
    super.initState();

    // Obtenemos la información del Pokémon
    PokeService().getPokemonInfo(widget.pokemon['id']).then((details) {
      setState(() {
        pokemonDetails = Future.value(details);
        pokemonType = details['types'];
      });

      // Obtenemos la cadena evolutiva del Pokémon
      return PokeService().getPokemonEvolutionChain(widget.pokemon['id']);
    }).then((evolutionChain) {
      setState(() {
        evolutionsList = evolutionChain;
      });
    }).catchError((error) {
      if (kDebugMode) {
        print('Error while getting pokemonDetails or evolutionsList: $error');
      }
      setState(() {
        pokemonType = 'Unknown';
      });
    });

    // Obtenemos los movimientos del Pokémon
    PokeService().getPokemonMoves(widget.pokemon['id']).then((moves) {
      setState(() {
        _moves = moves;
      });
    }).catchError((error) {
      if (kDebugMode) {
        print('Error while getting _moves: $error');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '#' '${widget.pokemon['id']}' +
              '  ' +
              '${widget.pokemon['name']}'.capitalize(),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.home),
            onPressed: () {
              Navigator.popUntil(
                  context, ModalRoute.withName(Navigator.defaultRouteName));
            },
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            GestureDetector(
              onTap: () {},
              child: Container(
                //imagen
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Image.network(
                      widget.pokemon['imageUrl'],
                      fit: BoxFit.cover,
                      width: 300,
                      height: 300, // Para ajustar la imagen al contenedor
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text(
                          '# ${widget.pokemon['id']} ' +
                              '${widget.pokemon['name']}'.capitalize(),
                          style: const TextStyle(fontSize: 15),
                        ),
                        Text(
                          ' ${widget.pokemon['types']}'.capitalize(),
                          style: const TextStyle(fontSize: 15),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(height: 20),
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Flexible(
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Características',
                            style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(height: 10),
                          FutureBuilder<Map<String, dynamic>>(
                            future: pokemonDetails,
                            builder:
                                (BuildContext context, AsyncSnapshot snapshot) {
                              if (snapshot.connectionState ==
                                  ConnectionState.waiting) {
                                return const Center(
                                  child: CircularProgressIndicator(),
                                );
                              } else if (snapshot.hasError) {
                                return Text(
                                  'Failed to load pokemon details: ${snapshot.error}',
                                );
                              } else if (snapshot.hasData) {
                                final details = snapshot.data!;
                                return Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Text(
                                      'Peso: ${details['weight']} Kg.',
                                      style: const TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(
                                      'Altura: ${details['height']} Cm',
                                      style: const TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(
                                      'Región: ${details['region']}',
                                      style: const TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                );
                              } else {
                                return const Text('No data available');
                              }
                            },
                          )
                        ],
                      ),
                    ),
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Movimientos',
                          style: TextStyle(
                              fontSize: 15, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(height: 10),
                        Expanded(
                          child: ListView.builder(
                            itemCount: _moves.length,
                            itemBuilder: (BuildContext context, int index) {
                              return ListTile(
                                title: Text('${_moves[index]}'),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            const Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Evoluciones',
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                )
              ],
            ),
            //evoluciones inicio
            SizedBox(
              height: 100,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: evolutionsList.length,
                itemBuilder: (context, index) {
                  final evolution = evolutionsList[index];

                  return SizedBox(
                    width: 150,
                    child: ListTile(
                      title: Text(
                        '${evolution['name'] ?? ''}',
                        style: const TextStyle(fontSize: 10),
                      ),
                      subtitle: Text(
                        '#${evolution['id'].toString()}', // Cambio aquí
                        style: const TextStyle(fontSize: 10),
                      ),
                      leading: CircleAvatar(
                        child: Image.network(
                          '${evolution['spriteUrl']}',
                          fit: BoxFit.cover,
                          errorBuilder: (context, error, stackTrace) =>
                              const Icon(Icons.error),
                        ),
                      ),
                      onTap: () async {
                        final evolutionDetails =
                            await PokeService().getPokemonInfo(evolution['id']);
                        final evolutionMoves = await PokeService()
                            .getPokemonMoves(evolution['id']);
                        Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => VistaPokemon(
                            pokemon: evolutionDetails,
                            moves: evolutionMoves,
                            vista: '',
                            pokemonNumber: evolutionDetails['id'],
                          ), 
                        ));
                      },
                    ),
                  );
                },
              ),
            ),
            //evoluciones fin
          ],
        ),
      ),
    );
  }
}
