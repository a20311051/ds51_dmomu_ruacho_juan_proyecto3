import 'package:flutter/material.dart';
import 'network/pokeService.dart';
import 'package:pokeapi/vistaPokemon.dart';

class Home extends StatefulWidget {
  const Home({
    Key? key,
    this.title = 'Poke Api',
    this.type = 'normal',
  }) : super(key: key);

  final String title;
  final String type;

  @override
  _HomeState createState() => _HomeState();
}

extension ExtensionCapital on String {
  String capitalize() {
    return '${this[0].toUpperCase()}${substring(1).toLowerCase()}';
  }
}

class _HomeState extends State<Home> {
  late Future<List<dynamic>> pokemonList;

  @override
  void initState() {
    super.initState();
    // Aquí se llama al método getPokemonList de PokeService para obtener una lista de pokemon.
    pokemonList =
        PokeService().getPokemonList(1008, type: widget.type, vista: '');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(onPressed: () {}, icon: const Icon(Icons.menu)),
        title: Text(widget.title),
      ),
      body: FutureBuilder<List<dynamic>>(
        future: pokemonList,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError || snapshot.data == null) {
            return const Center(child: Text('Failed to load Pokemon list'));
          } else {
            final pokemon = snapshot.data!;
            return ListView.builder(
              itemCount: pokemon.length,
              itemBuilder: (context, index) {
                final pokemonNumber =
                    pokemon[index]['id'].toString().padLeft(3, '0');
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => VistaPokemon(
                          pokemon: pokemon[index],
                          vista: '',
                          moves: const [],
                          pokemonNumber: 0,
                        ),
                      ),
                    );
                  },
                  child: ListTile(
                    leading: Image.network(pokemon[index]['imageUrl']),
                    title: Text(
                        '#$pokemonNumber ' '  ${pokemon[index]['name']} '
                            .capitalize()),
                    subtitle: Text('${pokemon[index]['types']}'.capitalize()),
                  ),
                );
              },
            );
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: const Text("No hay JAJAJAJJAJAJA"),
                actions: [
                  TextButton(
                    child: const Text("Cerrar"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          );
        },
        child: const Icon(Icons.search_outlined),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniEndDocked,
    );
  }
}
