import 'dart:convert' show json, jsonDecode;
import 'package:http/http.dart' as http;
import 'package:pokeapi/home.dart';

import '../data/models/pokemon.dart';

class PokeService {
  String baseUrl = 'https://pokeapi.co/api/v2';

  Future<List<dynamic>> getPokemonList(pokemonNumber,
      {required String type,
      int? selectedPokemonId,
      required String vista}) async {
    final response =
        await http.get(Uri.parse('$baseUrl/pokemon?offset=0&limit=1008'));
    final data = json.decode(response.body);

    List<dynamic> pokemonList = [];

    for (var pokemon in data['results'] ?? []) {
      final pokemonResponse = await http.get(Uri.parse(pokemon['url']));
      final pokemonData = json.decode(pokemonResponse.body);

      List<String> types = [];
      for (var type in pokemonData['types']) {
        types.add(type['type']['name']);
      }

      final pokemonInfo = {
        'id': pokemonData['id'],
        'name': pokemonData['name'],
        'imageUrl': pokemonData['sprites']['front_default'],
        'types': types.join('   -   '),
      };

      if (types.contains(type)) {
        if (pokemonData['id'] == selectedPokemonId) {
          final evolutionChain =
              await getPokemonEvolutionChain(selectedPokemonId!);
          pokemonInfo['selectedPokemon'] = {
            'name': evolutionChain[0]['name'],
            'id': evolutionChain[0]['id'],
            'imageUrl': evolutionChain[0]['imageURL'],
          };
        }
        pokemonList.add(pokemonInfo);
      }
    }

    return pokemonList;
  }


Future<Pokemon> getPokemonDetails(String nameOrId) async {
  final url = 'https://pokeapi.co/api/v2/pokemon/$nameOrId';
  final response = await http.get(Uri.parse(url));

  if (response.statusCode == 200) {
    final json = jsonDecode(response.body);
    final pokemon = Pokemon.fromJson(json);
    return pokemon;
  } else {
    throw Exception('Failed to load Pokemon details');
  }
}





  Future<List<Map<String, dynamic>>> getPokemonEvolutionChain(
      int pokemonId) async {
    final response =
        await http.get(Uri.parse('$baseUrl/pokemon-species/$pokemonId'));
    final data = json.decode(response.body);
    final evolutionChainUrl = data['evolution_chain']['url'];
    final response2 = await http.get(Uri.parse(evolutionChainUrl));
    final data2 = json.decode(response2.body);
    final chain = data2['chain'];
    final pokemonDataResponse =
        await http.get(Uri.parse('$baseUrl/pokemon/$pokemonId'));
    final pokemonData = json.decode(pokemonDataResponse.body);
    final List<Map<String, dynamic>> evolutionChain = [];

    // Verificar si la clave 'evolution_chain' existe
    if (data.containsKey('evolution_chain')) {
      await _parseChainRecursive(chain, evolutionChain, pokemonData);
    }

    return evolutionChain;
  }

  Future<void> _parseChainRecursive(
    Map<String, dynamic> chain,
    List<Map<String, dynamic>> evolutionChain,
    Map<String, dynamic> pokemonData,
  ) async {
    if (chain != null && evolutionChain != null && pokemonData != null) {
      final speciesName = chain['species']['name'];
      final speciesUrl = chain['species']['url'];
      final speciesId = int.parse(speciesUrl.split('/')[6]);
      final speciesResponse =
          await http.get(Uri.parse('$baseUrl/pokemon/$speciesId'));
      final speciesData = json.decode(speciesResponse.body);
      final List<String> types = [];
      for (var type in speciesData['types']) {
        types.add('${type['type']['name']}'.capitalize());
      }

      final spriteUrl = speciesData['sprites']['front_default'];
      evolutionChain.add({
        'name': speciesName,
        'url': speciesUrl,
        'id': speciesId,
        'spriteUrl': spriteUrl,
        'types': types.join('   '), // Agregar tipos del Pokémon
      });

      final evolvesTo = chain['evolves_to'];

      if (evolvesTo != null && evolvesTo.isNotEmpty) {
        for (var pokemon in evolvesTo) {
          await _parseChainRecursive(pokemon, evolutionChain, speciesData);
        }
      }
    }
  }

  Future<Map<String, dynamic>> getPokemonInfo(int id) async {
    final response = await http.get(Uri.parse('$baseUrl/pokemon/$id'));
    if (response.statusCode != 200) {
      throw Exception('Failed to load Pokemon information');
    }
    final data = json.decode(response.body);

    List<String> types = [];
    for (var type in data['types']) {
      types.add('${type['type']['name']}'.capitalize());
    }

    // Make a separate request to get the Pokemon's moves
    final movesResponse = await http.get(Uri.parse('$baseUrl/pokemon/$id'));
    if (movesResponse.statusCode != 200) {
      throw Exception('Failed to load Pokemon moves');
    }
    final movesData = json.decode(movesResponse.body);

    List<String> moves = [];
    for (var move in movesData['moves']) {
      moves.add('${move['move']['name']}'.capitalize());
    }

    try {
      final pokemonInfo = {
        'name': data['name'],
        'id': data['id'],
        'imageUrl': data['sprites']['front_default'],
        'types': types.join(' - '),
        'weight': data['weight'] / 10, // weight in kg
        'height': data['height'] * 10, // height in cm
        'region': getRegion(data['id']),
        'moves': moves.join(', '), // Add the moves to the pokemonInfo map
      };
      return pokemonInfo;
    } catch (e) {
      print('Error while decoding JSON: $e');
      throw Exception('Failed to decode Pokemon information');
    }
  }

  String getRegion(int id) {
    if (id >= 1 && id <= 151) {
      return "Kanto";
    } else if (id >= 152 && id <= 251) {
      return "Johto";
    } else if (id >= 252 && id <= 386) {
      return "Hoenn";
    } else if (id >= 387 && id <= 493) {
      return "Sinnoh";
    } else if (id >= 494 && id <= 649) {
      return "Teselia";
    } else if (id >= 650 && id <= 721) {
      return "Kalos";
    } else if (id >= 722 && id <= 809) {
      return "Alola";
    } else if (id >= 810 && id <= 905) {
      return "Galar";
    } else if (id >= 906 && id <= 1008) {
      return "Paldea";
    } else {
      return "Desconocido";
    }
  }

  Future<List<String>> getPokemonTypes(int typeId) async {
    final response = await http.get(Uri.parse('$baseUrl/type/$typeId'));
    if (response.statusCode == 200) {
      final decodedJson = jsonDecode(response.body);
      final types = decodedJson['results'].map<String>((type) {
        return '${type['name']}'.capitalize();
      }).toList();
      return types;
    } else {
      throw Exception('Failed to load Pokemon types');
    }
  }

  Future<List<dynamic>> getPokemonMoves(int pokemonId) async {
    final response = await http.get(Uri.parse('$baseUrl/pokemon/$pokemonId'));
    if (response.statusCode == 200) {
      final decodedJson = jsonDecode(response.body);
      final moves = decodedJson['moves'];
      final pokemonMoves = [];

      for (var move in moves) {
        final moveName = move['move']['name'];
        pokemonMoves.add(moveName);
      }

      return pokemonMoves;
    } else {
      throw Exception('Failed to load moves for this Pokemon');
    }
  }

  Future<List<dynamic>> getMachineList(int machineId) async {
    final response = await http.get(Uri.parse('$baseUrl/machine/$machineId'));
    if (response.statusCode == 200) {
      final decodedJson = jsonDecode(response.body);
      final machineList = decodedJson['results'];
      List<dynamic> machines = [];
      for (var machine in machineList) {
        final machineId = machine['url']
            .split('/')
            .last; // Obtener el ID de la máquina del URL
        machines.add(machineId);
      }

      return machines;
    } else {
      throw Exception('Failed to load machine list');
    }
  }

  static getRegionData(String region) {}

  static getMovesData({required power}) {}
}
