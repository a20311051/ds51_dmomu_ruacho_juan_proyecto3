import 'package:flutter/material.dart';
import 'network/pokeService.dart';

class Move extends StatefulWidget {
  Move({Key? key, required this.move}) : super(key: key);

  final int move;
  final List<dynamic> moves = [];

  @override
  State<Move> createState() => _MoveState();
}

class _MoveState extends State<Move> {
  @override
  void initState() {
    super.initState();
    _getMoves();
  }

  Future<void> _getMoves() async {
    final moves =
        await PokeService().getPokemonMoves(widget.move.toString() as int);
    setState(() {
      widget.moves.addAll(moves);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Movimientos:"),
      ),
      body: Center(
        child: widget.moves.isEmpty
            ? const CircularProgressIndicator()
            : ListView.builder(
                itemCount: widget.moves.length,
                itemBuilder: (context, index) {
                  final move = widget.moves[index];
                  return ListTile(
                    title: Text(move['move']['name']),
                  );
                },
              ),
      ),
    );
  }
}